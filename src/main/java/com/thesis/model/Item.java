package com.thesis.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Item {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	private long orderid;

	private long flowerid;

	private int orderedammount;

	private int suppliedammount;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getOrderid() {
		return orderid;
	}

	public void setOrderid(long orderid) {
		this.orderid = orderid;
	}

	public long getFlowerid() {
		return flowerid;
	}

	public void setFlowerid(long flowerid) {
		this.flowerid = flowerid;
	}

	public int getOrderedammount() {
		return orderedammount;
	}

	public void setOrderedammount(int orderedammount) {
		this.orderedammount = orderedammount;
	}

	public int getSuppliedammount() {
		return suppliedammount;
	}

	public void setSuppliedammount(int suppliedammount) {
		this.suppliedammount = suppliedammount;
	}

}
