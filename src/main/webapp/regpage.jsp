<!DOCTYPE html>
<html>
	<head>
	<style>
		.main {
			border-radius: 25px;
			border-style: solid;
			border-width: 2px;
			margin-left: 5%;
			margin-right: 5%;
			margin-top: 2%;
			box-shadow: 0 10px 10px 0 rgba(0, 0, 0, 0.2), 0 10px 20px 0 rgba(0, 0, 0, 0.19);
			height: 100%;
		}
		
		body {
			background-color: #C2EE6B;
			height: 100%;
			min-height: 100%;
		}
		
		.box {
			width: 40%;
			height: 5%;
			margin-left: 3%;
			margin-top: 8%;
			margin-bottom: 3%;
			font-size: 18px;
			display: inline-block;
		}
		
		.boxtext {
			width: 40%;
			height: 5%;
			margin-left: 3%;
			margin-top: 8%;
			margin-bottom: 3%;
			font-size: 18px;
			display: inline-block;
			text-align: right;
		}
		
		
	</style>
	<title></title>
	</head>
	<body>
		<div class="main">
			<%@include  file="unreg.html" %>
			<form method="post" >
				<div class="boxtext"><strong>Felhasználónév:</strong></div>
				<div class="box"><input type="text" name="username" size="15"></div>
				<div class="boxtext"><strong>Jelszó:</strong></div>
				<div class="box"><input type="text" name="password" size="15"></div>
				<div class="boxtext"><strong>Jelszó megersítése:</strong></div>
				<div class="box"><input type="text" name="passagain" size="15"></div>
				<div class="boxtext"><strong>Név:</strong></div>
				<div class="box"><input type="text" name="name" size="80"></div>
				<div class="boxtext"><strong>E-mail:</strong></div>
				<div class="box"><input type="text" name="email" size="80"></div>
				<div class="boxtext"><strong>Telefonszám:</strong></div>
				<div class="box"><input type="text" name="phone" size="25"></div>
				<div class="boxtext"><input type="submit" value="Regisztráció"></div>
				<div class="box"><input type="reset" value="Törlés"></div>
			</form>
		</div>
	</body>
</html>