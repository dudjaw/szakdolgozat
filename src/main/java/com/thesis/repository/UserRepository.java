package com.thesis.repository;

import org.springframework.data.repository.CrudRepository;

import com.thesis.model.User;

public interface UserRepository extends CrudRepository<User, Long> {

	User findByUsername(String uname);

}
